package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.entity.Student;

public class StudentDao extends BaseDao {

	public Student findStudent(String name) {
		Connection con = getConnection();
		PreparedStatement ps = null;
		Student student = null;
		// sql��䢘
		String sql = "select * from Student where name=?";
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			rs = ps.executeQuery();
			while(rs.next()) {
				student = new Student();
				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setSex(rs.getString("sex"));
				student.setDpt_id(rs.getInt("dpt_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(con, ps, rs);
		}
		return student;
	}
	
	public boolean addstudent(Student student) {
		// TODO Auto-generated method stub
		  int i=0;
			Object[] params = {student.getName(),student.getSex(),student.getDpt_id() };
			String sql = "insert into Student values (?,?,?)";
			try {
				conn = this.getConnection();
				pstmt = conn.prepareStatement(sql);				
				i=this.exceuteUpdate(sql, params);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				this.closeAll(conn, pstmt, rs);
			}
			return i>0;
	}
	public List<Student> queryAll() {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		PreparedStatement ps = null;
		List<Student> list = new ArrayList<Student>();
		// sql��䢘
		String sql = "select * from Student";
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Student student=new Student();
				student = new Student();
				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setSex(rs.getString("sex"));
				student.setDpt_id(rs.getInt("dpt_id"));
				list.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeAll(con, ps, rs);
		}
		return list;
	}
}