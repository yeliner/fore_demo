package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
/**
 *������ݿ�
 * @author 
 */
public class BaseDao {
private static String driver = 
		"com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static String url = 
		"jdbc:sqlserver://localhost:1433;DatabaseName=mobile";
	private static String user = "sa";
	private static String password = "123.com"; 

	protected Connection conn;
	protected PreparedStatement pstmt;
	protected ResultSet rs;
	
	public Connection getConnection() {
		Connection conn = null;
		// 获取连接并捕获异
		try {
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);	
			} catch (Exception e) {
				e.printStackTrace();// 异常处理
			}
	
		return conn;// 返回连接对象
	}
	
	public void closeAll(Connection conn, Statement stmt, ResultSet rs) {
		// 若结果集对象不为空，则关
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 若Statement对象不为空
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 若数据库连接对象不为空
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public int exceuteUpdate(String sql,Object...prams){
		int result=0;
		conn=this.getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			for(int i=0;i<prams.length;i++){
				pstmt.setObject(i+1, prams[i]);	
			}
			result=pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			closeAll(conn, pstmt, rs);
		}
		return result;
	}
}
