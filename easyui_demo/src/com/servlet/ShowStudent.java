package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.entity.Student;
import com.google.gson.Gson;

public class ShowStudent extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public ShowStudent() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		StudentDao sd = new StudentDao();
		List<Student> list=new ArrayList<Student>();
	/*	//测试数据		
		list.add(new Student( 1,"张三丰1", "男", 100));
		list.add(new Student(2,"张三丰2", "男", 100));
		list.add(new Student( 3,"张三丰3", "男", 100));
		list.add(new Student( 4,"张三丰4", "男", 100));
		list.add(new Student( 5,"张三丰5", "男", 100));
		list.add(new Student(6,"张三丰6", "男", 100));
		list.add(new Student(7,"张三丰7", "男", 100));
		list.add(new Student(8,"张三丰8", "男", 100));*/
		try {
			list = sd.queryAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}		
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		out.print(gson.toJson(list));
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
