package com.entity;

public class Student {
	private Integer id;
	private String name;
	private String sex;
	private Integer dpt_id;

	public Student() {
		super();
	}

	public Student(String name, String sex, Integer dpt_id) {
		super();
		this.name = name;
		this.sex = sex;
		this.dpt_id = dpt_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getDpt_id() {
		return dpt_id;
	}

	public void setDpt_id(Integer dpt_id) {
		this.dpt_id = dpt_id;
	}

	public Student(Integer id, String name, String sex, Integer dpt_id) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.dpt_id = dpt_id;
	}


}
