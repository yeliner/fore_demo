<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
</head>

<script type="text/javascript">
	$(function() {
		/*  拖动示例 */
		$('#draggable_demo').draggable({
			handle : '#draggable_content'
		});

		/* 可变大小的窗口 */
		$('#droppable_demo').resizable({
			maxWidth : 800,
			maxHeight : 600
		});

		/* 	 查询框 */
		$('#ss').searchbox({
			searcher : function(value, name) {
				alert(value + "," + name+"进度条加10");
				//搜索一次给进度条设置value+10
				var value = $('#p').progressbar('getValue');
				value = value < 100 ? value + 10 : 0;
				$('#p').progressbar('setValue', value);

			},
			menu : '#mm',
			prompt : 'Please Input Value'
		});

		/* 	进度条 */
		$('#p').progressbar({
			value : 0
		});
		//给droppable_demo加提示框
		$('#droppable_demo').tooltip({
			position : 'bottom',
			content : '可拖动，可变化',
			onShow : function() {
				$(this).tooltip('tip').css({
					backgroundColor : '#666',
					borderColor : '#666'
				});
			}
		});
	});
</script>
<body>

	<!-- 查询框 -->
	<input id="ss" style="width:300px" />
	<div id="mm" style="width:150px">
		<div data-options="name:'item1'">Search Item1</div>
		<div data-options="name:'item2'">Search Item2</div>
		<div data-options="name:'item3'">Search Item3</div>
	</div>

	<!-- 	进度条 -->
	<div id="p" style="width:400px;"></div>

	<!-- 拖动示例 -->
	<div id="draggable_demo">
		<div id="draggable_content">
			<!-- 放置区 -->
			<div id="droppable_demo" class="easyui-droppable"
				data-options="accept:'#draggable_content'"
				style="width:400px;height:100px;border:1px red solid"></div>
		</div>
	</div>

</body>
</html>
