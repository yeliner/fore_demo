<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
</head>

<script type="text/javascript">
	$(function() {
		$('#dlg').dialog({
			width : 400,
			height : 200,
			resizable : true,
			closed : true,
			modal : true,
			buttons : '#optButtons'
		});
		$('a').linkbutton({});
		$('#dpt_id').combobox({
			url : 'com_data.json',
			valueField : 'id',
			textField : 'text'
		});

	});

	var url = "";
	//点击按钮， 打开对话框 
	function openDlg() {
		$("#dlg").dialog("open").dialog("setTitle", "添加学生");
		url = "addStudent";
	}
	//保存操作
	function saveDlg() {
		$('#frm').form("submit",{
			url : url,
			onSubmit : function() {
				//在提交的时候做一些简单的验证
				var name = $("#name").val();
				var sex = $("#sex").val();
				if (name == "" || sex == "") {
					alert("请把信息补充完整");					
					return false;
				}
			},
			success : function(data) {
				alert(data);
				$("#dlg").dialog("close");
			}
		});

	}
	//取消操作
	function celDlg() {
		$("#dlg").dialog("close");
	}
</script>
<body>

	<div>
		<a href="javascript:openDlg()">添加学生</a>
	</div>
	<div id="dlg">
		<form id="frm" method="post">
			<table>
				<tr>
					<td>姓名：</td>
					<td><input id="name" name="name" class="easyui-validatebox" />
					</td>
				</tr>
				<tr>
					<td>性别：</td>
					<td><input id="sex" name="sex" class="easyui-validatebox" />
					</td>
				</tr>
				<tr>
					<td>部门：</td>
					<td><input id="dpt_id" name="dpt_id"
						class="easyui-validatebox" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="optButtons">
		<a href="javascript:saveDlg()" iconCls="icon-ok">保存</a> <a
			href="javascript:celDlg()" iconCls="icon-cancel">取消</a>
	</div>

</body>
</html>
