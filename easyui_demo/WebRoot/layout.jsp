<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
</head>

<script type="text/javascript">
	$(function() {
		/* 面板 */
		$('#p').panel({
			closable : true,
			collapsible : true,
			minimizable : true,
			maximizable : true,
			//添加save，add
			tools : [ {
				iconCls : 'icon-add',
				handler : function() {
					alert('new');
				}
			}, {
				iconCls : 'icon-save',
				handler : function() {
					alert('save');
				}
			} ],
			//被选择
			onSelect : function(title) {
				alert(title + ' is selected');
			}
		});
	});
	function addtab() {
		$('#tt').tabs('add', {
			title : 'New Tab',
			content : 'Tab Body',
			closable : true,
			tools : [ {
				iconCls : 'icon-mini-refresh',
				handler : function() {
					alert('refresh');
				}
			} ]
		});
	};
</script>
<body>
	<div id="p" title="My Panel"
		style="width:500px;height:150px;padding:10px;background:#fafafa;">
		<div>
			<input type="button" value="addtab" title="addTab"
				onclick="addtab();" />
		</div>
		<div id="tt" class="easyui-tabs" style="width:500px;height:250px;">
			<div title="Tab1" style="padding:20px;display:none;">tab1</div>
			<div title="Tab2" data-options="closable:true"
				style="overflow:auto;padding:20px;display:none;">tab2</div>
			<div title="Tab3" data-options="iconCls:'icon-reload',closable:true"
				style="padding:20px;display:none;">tab3</div>
		</div>

	</div>

</body>
</html>
