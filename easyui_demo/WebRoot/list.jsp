<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
</head>

<script type="text/javascript">
	$(function() {
	
		 $("#dg").datagrid({             
       panelHeight: "auto",
       title: "学生列表",
       iconCls: "icon-edit",
       striped: true,    
       rownumbers: true,
       loadMsg: "正在加载数据,请稍等。。。",
       pageList: [5, 10, 18],
       pageSize: 5,
       pagination:true,   
       url: "ShowStudent",
       width:1400,
       height:600,
       columns: [
        [
          { title: '全选',  align: 'center', width: 60 },
          { title: '基本信息', colspan: 2, align: 'center', width: 460 },
          { title: '详细信息', colspan: 3, align: 'center', width: 600 }         
        ],

        [
          { field: "ck", checkbox:true, width: 100, align: "center" },
          { field: "id", title: "学号", width: 200, align: "center",
            formatter: 
            	function (value, rowdata, rowIndex) {
                return "<a href='javascript:update("+value+")'>" + value + "</a>";
               }

           },
          { field: "name", title: "姓名", width: 200, align: "center" },
          { field: "sex", title: "性别", width: 200, align: "center"},          
          { field: "dpt_id", title: "院系", width: 200, align: "center" }
        ]
      ]
   });
		 

	});
</script>
<body>

	<div>
		<table id="dg"></table>  		
	</div>

</body>
</html>
