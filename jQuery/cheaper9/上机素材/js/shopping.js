$(function(){
	$("#shopping_commend_arrow").toggle(hide(),show());
	$("#removeAllProduct").click(deleteAll);
	$(".shopping_product_list_6 a").click(deletesingle);
	set();
	jisuan();
	})
//显示隐藏功能  
function show(){
	$("#shopping_commend_arrow").attr("src","images/shopping_arrow_up.gif");
	$("#shopping_commend_sort").show();
	}
function hide(){
	$("#shopping_commend_arrow").attr("src","images/shopping_arrow_down.gif");
	$("#shopping_commend_sort").hide();
	}
//清空功能
function deleteAll(){
	var flag=confirm("确定要清空购物车？");
	if(flag){$("#myTableProduct").remove();}
	else{alert("该操作已取消！");}
			
	}
//删除单行功能
function deletesingle(){
	var flag=confirm("确定要清空购物车？");
	if(flag){$(this).parent().parent().remove();}
	else{alert("该操作已取消！");}
	
	}
//修改数量功能
function set(){
	$(".shopping_product_list_5 :input[type='text']").change(function(){
		jisuan();
		alert("您的购买数量已修改 您共节省金额 "+$("#product_save").text()+"    可获商品积分 "+$("#product_integral").text()+ "    商品金额总计 "+$("#product_total").text());
	});
	}
//计算money和积分
function jisuan(){
	var saveMoney=0;
	var getjf=0;
	var allMoney=0;
	$("#myTableProduct tr").each(function(i,e){
		var tr=$(e);
		var jf= tr.find(".shopping_product_list_2 label").text();//单品积分
		
		var sprice=parseInt(tr.find(".shopping_product_list_3 label").text());//市场价格
		
		var dprice=parseInt(tr.find(".shopping_product_list_4 label").text());//当前面价格
		var num=parseInt(tr.find(".shopping_product_list_5 :input[type='text']").val());//数量
		saveMoney+=(sprice-dprice)*num;//节省的总金额
	    getjf+=jf*num;//可获积分
		allMoney+=dprice*num;//总金额
		$("#product_save").text(saveMoney);//显示节省的总金额
		$("#product_integral").text(getjf);//显示可获积分
		$("#product_total").text(allMoney);//显示总金额
		
		})
	}
	
