// 创建XMLHttpRequest对象
var xmlhttp=null;
function GetXmlHttpObject()//封装一个兼容浏览器的ajax函数
{
  if(window.XMLHttpRequest){
      xmlhttp=new XMLHttpRequest();
    if(xmlhttp.overrideMimeType){
        xmlhttp.overrideMimeType("text/xml");
    }
}else{                  
       xmlhttp=new ActiveXObject("Msxml2.XMLHttp");
   }
  return xmlhttp;
}


function chkname(form){
	if(form.name.value==""){
		name1.innerHTML="<font color=#FF0000>请输入用户名！</font>";
	}else{
		var user = form.name.value;
		var url = "chkname.php?user="+user;
		xmlhttp = GetXmlHttpObject();//调用函数GetXmlHttpObject()
		xmlhttp.open("GET",url,true);
		xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4){
				var msg = xmlhttp.responseText;
				if(msg == '2'){
					name1.innerHTML="<font color=#FF0000>用户名被占用！</font>";
					return false;
				}else if(msg == '1'){
					name1.innerHTML="<font color=green>恭喜您，可以注册!</font>";
					form.c_name.value = "yes";
					
				}else{
					name1.innerHTML="<font color=green>"+msg+"</font>";
				}
			}
		}
		xmlhttp.send(null);
	}
}
function chkinput(form){
	if(form.name.value==""){
		form.name.focus();
		return false;
	}
 
	
	if(form.pwd1.value==""){
		form.pwd1.focus();
		return false;
	}
	if(form.pwd2.value==""){
		form.pwd2.focus();
		return false;
	}
	if(form.pwd2.value!=form.pwd1.value){
		form.pwd1.select();
		return false;
	}
	if(form.pwd1.value.length<6){
		form.pwd1.select();
		return false;
	}
 
	if(form.realname.value==""){
		form.realname.focus();
		return false;
	}
 
 
	var i=form.email.value.indexOf("@");
	var j=form.email.value.indexOf(".");
	if((form.email.value != "") && ((i<0)||(i-j>0)||(j<0))){
		form.email.select();
		return false;
	}
 
	 
 
	if(form.yzm.value==""){
		form.yzm.focus();
		return false;
	}
	if(form.yzm.value!=form.yzm2.value){
		alert("效验码输入错误！");
		form.yzm.focus();
		return false;
	}
}

function chkpwd1(form){
	if(form.pwd1.value==""){
		 alert("密码不能为空！！！");  
	}else if(form.pwd1.value.length<6){
		alert("注册密码长度应大于6位");  
	}else{
		 return true;
	}
}
function chkpwd2(form){
	if(form.pwd2.value==""){
		alert("确认密码不能为空！！！"); 
	}else if(form.pwd2.value.length<6){
		alert("确认密码长度应大于6位！");  
	}else if(form.pwd1.value!=form.pwd2.value){
		alert("注册密码与确认密码不同！"); 
	}else{
		 return true;
	}
}
 
function chkrealname(form){
	if(form.realname.value==""){
		alert("请输入真实姓名！"); 
	}else{
		 return true;
	}
}
 
function chkemail(form){
	var i=form.email.value.indexOf("@");
	var j=form.email.value.indexOf(".");
	if((form.email.value != "") && ((i<0)||(i-j>0)||(j<0))){
		alert("请输入正确的E-mail地址！"); 
	}else{
		 return true;
	}
}
 
function chkyzm(form){
	if(form.yzm.value==""){
		yzm1.innerHTML="<font color=#FF0000>请输入效验码！</font>"; 
	}else if(form.yzm.value!=form.yzm2.value){
		alert("yzm2===="+form.yzm2.value);
		alert("yzm===="+form.yzm.value);
		yzm1.innerHTML="<font color=#FF0000>效验码输入错误!</font>";
	}else{
		yzm1.innerHTML="<font color=green>输入正确</font>";
	}
}
function yzm(form){
	var num1=Math.round(Math.random()*10000000);
	var num=num1.toString().substr(0,3);
	document.write("<img name=codeimg src='yzm.php?num="+num+"'>");
	form.yzm2.value=num;
}
function code(form){
	var num1=Math.round(Math.random()*10000000);
	var num=num1.toString().substr(0,3);
	document.codeimg.src="yzm.php?num="+num;
	form.yzm2.value=num;
}