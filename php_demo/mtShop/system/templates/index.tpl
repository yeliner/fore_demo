<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>美淘网购物商城</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/table.css" />
<link rel="stylesheet" href="css/nominate.css" />
<link rel="stylesheet" href="css/pub.css" />
<link rel="stylesheet" href="css/reg.css"/>
<link rel="stylesheet" href="css/global.css"/>
<link rel="stylesheet" href="css/search.css"  />
<link rel="stylesheet" href="css/link.css" />
<link rel="stylesheet" type="text/css" href="css/image.css" />
<link rel="stylesheet" type="text/css" href="css/login.css"/>
<link rel="stylesheet" type="text/css" href="css/top.css" />
</head>
<script language="javascript" src="js/createxmlhttp.js"></script>
<script language="javascript" src="js/links.js"></script>
<script language="javascript" src="js/showcommo.js"></script>
<script language='javascript' src='js/queryform.js'></script>
<script language="javascript" src="js/shopcar.js"></script>
<script language="javascript" src="js/settle.js"></script>
<script language="javascript" src="js/search.js"></script>
<script language="javascript" src="js/info.js"></script>
<script language="javascript" src="js/login.js"></script>
<script language="javascript" src="js/top.js"></script>
<script language="javascript" src="js/image.js"></script>
<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td>
	    	{include file='top.tpl'}
	    </td>
	  </tr>
  </table>
  <!--左边部分-->
  <div id="divleft">
  	<!--网站公告-->
  	{include file='public.tpl'}
 	<!--用户登录-->
 	{include file='login.tpl'}
 	<!--客服热线-->
	{include file='links.tpl'}
  </div>
  <!--右边部分-->
  <div id="divright">
  	{include file='search.tpl'}
	{include file=$admin_phtml}
  </div>
  
  <!--尾部部分-->
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td>{include file='buttom.tpl'}</td>
    </tr>
 </table>
 
</body>
</html>
