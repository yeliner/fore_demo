<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 让对移动设备友好 -->
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<!-- 	环境安装 -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<body>
	<h1>Hello, world!</h1>
	<div class="container">
		<div class="row">


			<div class="col-xs-6 col-sm-6"
				style="background-color: #dedee6;
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
				<p>a</p>
				<table class="table table-striped">
					<caption>条纹表格布局</caption>
					<thead>
						<tr>
							<th>名称</th>
							<th>城市</th>
							<th>邮编</th>
						</tr>
					</thead>
					<tbody>
						<tr class="active">
							<td>产品1</td>
							<td>23/11/2013</td>
							<td>待发货</td>
						</tr>
						<tr class="success">
							<td>产品2</td>
							<td>10/11/2013</td>
							<td>发货中</td>
						</tr>
						<tr class="warning">
							<td>产品3</td>
							<td>20/10/2013</td>
							<td>待确认</td>
						</tr>
						<tr class="danger">
							<td>产品4</td>
							<td>20/10/2013</td>
							<td>已退货</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="clearfix visible-xs"></div>

			<div class="col-xs-6 col-sm-6"
				style="background-color: #dedef8;box-shadow: 
        inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
				<p>b</p>
				<div class="row">
					<div class="col-xs-6 col-sm-6 lead text-left"
						style="background-color: #dedef8;
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
						<p>b1</p>
						<ul>
							<li>Item 1</li>
							<li>Item 2</li>
							<li>Item 3</li>
							<li>Item 4</li>
						</ul>
					</div>
					<div class="col-xs-6 col-sm-6 "
						style="background-color: #dedef8;box-shadow: 
        inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
						<p>b2</p>
						<ol>
							<li>Item 1</li>
							<li>Item 2</li>
							<li>Item 3</li>
							<li>Item 4</li>
						</ol>

					</div>


				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>
