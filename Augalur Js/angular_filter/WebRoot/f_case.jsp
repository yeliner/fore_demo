<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script src="angular.min.js">
	
</script>
<script src="f_controller.js">
	
</script>
</head>

<body ng-app="case_app" ng-controller="case_c">
	This is my f_case page.
<div>请输入文本:<input type="text" ng-model="text" /></div>
<div>默认:{{text}}</div>
<div>忽略大写:{{text|uppercase}}</div>
<div>忽略小写:{{text|lowercase}}</div>
</body>
</html>
