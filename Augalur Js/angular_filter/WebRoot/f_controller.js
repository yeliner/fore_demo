var filter_app = angular.module("filter_app", []);
filter_app.controller("filter_c", function($scope) {
	$scope.persons = [ {
		"name" : "jack",
		"age" : 1
	}, {
		"name" : "tom",
		"age" : 2
	}, {
		"name" : "anla",
		"age" : 3
	}, {
		"name" : "boby",
		"age" : 4
	} ];
});
var currency_app = angular.module("currency_app", []);
currency_app.controller("currency_c", function($scope) {
	$scope.amount = 1234.55;
});
var number_app = angular.module("number_app", []);
number_app.controller("number_c", function($scope) {
	$scope.num = 1234.55;
});
var case_app = angular.module("case_app", []);
case_app.controller("case_c", function($scope) {
	$scope.text = "hello world";
});
var date_app = angular.module("date_app", []);
date_app.controller("date_c", function($scope) {
	$scope.now = new Date();
});

var json_app = angular.module("json_app", []);

json_app.controller("json_c", function($scope) {
	$scope.person = new Person("jack", 12);
});
function Person(name, age) {
	this.name = name;
	this.age = age;
}

var limit_app = angular.module("limit_app",[]);
limit_app.controller("limit_c",function($scope){
$scope.persons=[
{name:"Smith",age:23},
{name:"Jane",age:30},
{name:"Jack",age:32},
{name:"Michael",age:40}
];
$scope.message ="hello!123344";
});


var order_app = angular.module("order_app",[]);
order_app.controller("order_c",function($scope){
$scope.persons=[
{name:"Smith",age:33},
{name:"Jane",age:43},
{name:"Jack",age:32},
{name:"Michael",age:40}
];
});

