<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script src="angular.min.js">
	
</script>
<script src="f_controller.js">
	
</script>
</head>

<body ng-app="json_app" ng-controller="json_c">
	This is my f_json page.
<!-- 	<div>
		请输入姓名:<input type="text" name="name" />
	</div>
	<div>
		请输入年龄:<input type="number" name="age" />
	</div> -->
	<div>json:{{person|json}}</div>
</body>
</html>
