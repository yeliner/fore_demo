<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'f_filter.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

<script src="angular.min.js">
	
</script>
<script src="f_controller.js">
	
</script>
</head>

<body ng-app="filter_app">
	This is my f_filter page.
	<br>
	<table ng-controller="filter_c" border="1">
		<tr>
			<td>请输入过滤信息:</td>
			<td><input type="text" ng-model="personInfo" />
			</td>
		</tr>
		<tr ng-repeat="person in persons|filter:personInfo">
			<td>{{person.name}}</td>
			<td>{{person.age}}</td>
		</tr>
	</table>
</body>
</html>
