<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript" src="angular.min.js"></script>

</head>

<body ng-app="">

	表单验证:
	<form name="user">
		<div>
			<input type="text" name="name" ng-model="name" ng-minlength="2" ng-maxlength="5" required /> <span
				ng-show="user.name.$error.required">用户名不能为空</span> <span
				ng-show="user.name.$error.minlength">用户名的长度不能小于2位</span> <span
				ng-show="user.name.$error.maxlength">用户名的长度不能大于4位</span><br> <input
				type="email" name="email" ng-model="email" required /> <span
				ng-show="user.email.$invalid">邮箱格式不正确</span> <span
				ng-show="user.email.$error.required">邮箱不能为空</span>
		</div>
	</form>
</body>
</html>
