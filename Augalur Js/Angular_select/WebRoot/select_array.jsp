<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>

</head>

<body ng-app="array_app" ng-controller="array_c">

	使用ng-options:
	<select ng-model="selectedName" ng-init="selectedName=names[0]"
		ng-options="x for x in names"></select>

	<br /> 使用ng-repeat:
	<select>
		<option ng-repeat="x in names">{{x}}</option>
	</select>


</body>
</html>
