var array_app = angular.module("array_app", []);
array_app.controller("array_c", function($scope) {
	$scope.names = [ "jack", "tom", "anla", "boby" ];
});

var project_app = angular.module("project_app", []);
project_app.controller("project_c", function($scope) {
	$scope.persons = [ {
		"name" : "jack",
		"age" : 1
	}, {
		"name" : "tom",
		"age" : 2
	}, {
		"name" : "anla",
		"age" : 3
	}, {
		"name" : "boby",
		"age" : 4
	} ];
});

var route_app = angular.module("route_app", [ 'ngRoute' ]).controller(
		'baidu_c', function($scope, $route) {
			$scope.$route = $route;
		}).controller('sina_c', function($scope, $route) {
	$scope.$route = $route;
}).config(function($routeProvider) {
	$routeProvider.when('/baidu', {
		templateUrl : '/baidu.jsp',
		controller : 'baidu_c'
	}).when('/sina', {
		templateUrl : '/sina.jsp',
		controller : 'sina_c'
	}).otherwise({
		redirectTo : '/baidu'
	});
});
