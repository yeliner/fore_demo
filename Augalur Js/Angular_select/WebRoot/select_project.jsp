<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>
</head>

<body ng-app="project_app" ng-controller="project_c">
	<div>
		使用ng-options: <select ng-model="selectedPerson1"
			ng-options="y.name for (x,y) in persons" ng-init="selectedPerson1=persons[0]"></select>
		<div>姓名: {{selectedPerson1.name}}</div>
		<div>年龄: {{selectedPerson1.age}}</div>
	</div>
	<div>
		使用ng-repeat: <select ng-model="selectedPerson2">
			<option ng-repeat="x in persons" value="{{x.age}}">{{x.name}}</option>
		</select>
		<div>年龄: {{selectedPerson2}}</div>
	</div>
</body>
</html>
