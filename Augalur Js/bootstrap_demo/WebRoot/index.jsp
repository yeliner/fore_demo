<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 让对移动设备友好 -->
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<!-- 	环境安装 -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery-1.8.3.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<body>
	<h1 class="text-center">Hello, world!</h1>
	<div class="container"
		style="background-color: #dedee6;
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Crikey</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"> 注册</a>
				</li>
				<li><a href="#"> 登录</a>
				</li>
			</ul>
		</div>
		</nav>
		<div class="row">


			<div class="col-xs-6 col-sm-3"
				style="
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
				<div class="col-md-offset-2 col-md-10">
					<p>login</p>
				</div>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<img src="img/timg.jpg" class="img-thumbnail img-circle"
								style="width: 100;height: 100">
						</div>
					</div>
					<div class="form-group">
						<label for="userName" class="col-md-4 control-label">用户名</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="userName"
								placeholder="请输入用户名">
						</div>
					</div>
					<div class="form-group">
						<label for="pwd" class="col-md-4 control-label">密码</label>
						<div class="col-md-8">
							<input type="password" class="form-control" id="pwd"
								placeholder="请输入密码">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<div class="checkbox">
								<label> <input type="checkbox">请记住我 </label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn btn-primary">登录</button>
						</div>
					</div>
				</form>


			</div>
			<div class="clearfix visible-xs"></div>

			<div class="col-xs-6 col-sm-9"
				style="
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
				<p>b</p>
				<div class="row">





					<div class="col-xs-6 col-sm-8 lead text-left"
						style="
            box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;">
						<p>a</p>
						<table class="table table-striped">
							<caption>条纹表格布局</caption>
							<thead>
								<tr>
									<th>名称</th>
									<th>城市</th>
									<th>邮编</th>
								</tr>
							</thead>
							<tbody>
								<tr class="active">
									<td>产品1</td>
									<td>23/11/2013</td>
									<td>待发货</td>
								</tr>
								<tr class="success">
									<td>产品2</td>
									<td>10/11/2013</td>
									<td>发货中</td>
								</tr>
								<tr class="warning">
									<td>产品3</td>
									<td>20/10/2013</td>
									<td>待确认</td>
								</tr>
								<tr class="danger">
									<td>产品4</td>
									<td>20/10/2013</td>
									<td>已退货</td>
								</tr>
							</tbody>
						</table>
					</div>






					<div class="col-xs-6 col-sm-4 "
						style="
            box-shadow: inset 1px 1px 1px #444, inset 1px 1px 1px #444;">

						<a href="#" class="list-group-item">常见问题 </a> <a href="#"
							class="list-group-item">注册问题</a> <a href="#"
							class="list-group-item">登录问题</a> <a href="#"
							class="list-group-item">客服中心</a>


					</div>


				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>
