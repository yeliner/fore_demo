package action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ModelDriven;

import services.UserServices;

import entity.UserInfo;

public class RegistAction implements ModelDriven<UserInfo> {
	private UserInfo userInfo = new UserInfo();
	private UserServices us;

	public UserServices getUs() {
		return us;
	}

	public void setUs(UserServices us) {
		this.us = us;
	}
	public UserInfo getModel() {
		// TODO Auto-generated method stub
		return userInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public void execute() throws IOException{
		if (us.findUser(userInfo.getName())==null) {		
			print("regist success");
		}else {
			print("regist fail");
		}
	}
	
	private void print(String str) throws IOException {
		HttpServletResponse rep = ServletActionContext.getResponse();
		rep.setContentType("text/json;charset=utf-8");
		PrintWriter pw = rep.getWriter();
		pw.write(str);
		pw.flush();
		pw.close();
	}


	}

	

