package action;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import services.UserServices;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;


import entity.UserInfo;

public class LoginAction implements ModelDriven<UserInfo> {
	private UserServices us;
	
	public UserServices getUs() {
		return us;
	}

	public void setUs(UserServices us) {
		this.us = us;
	}

	private UserInfo userInfo = new UserInfo();

	public UserInfo getModel() {
		// TODO Auto-generated method stub
		return userInfo;
	}

	public void execute() throws IOException{
		if (us.findUser(userInfo.getName(), userInfo.getPwd())!=null) {		
			print("login success");
		}else {
			print("login fail");
		}
	}
	
	private void print(String str) throws IOException {
		HttpServletResponse rep = ServletActionContext.getResponse();
		rep.setContentType("text/json;charset=utf-8");
		PrintWriter pw = rep.getWriter();
		pw.write(str);
		pw.flush();
		pw.close();
	}


	

}
