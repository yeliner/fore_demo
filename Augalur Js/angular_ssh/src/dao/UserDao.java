package dao;

import java.util.List;


import dao.base.BaseDAO;

import entity.UserInfo;

public class UserDao extends BaseDAO {
	List list;

	public UserInfo findUser(String name, String pwd) {
		String hql = "from UserInfo where name=? and pwd=?";
		Object[] params = { name, pwd };
		list = this.searchByHQL(hql, params);
		if (list.size() > 0) {
			return (UserInfo) list.get(0);
		}
		return null;
	}

	public UserInfo findUser(String name) {
		// 注册时不能重名
		// hql语句
		String hql = "from UserInfo where name=?";
		Object[] param = { name };
		list = this.searchByHQL(hql, param);
		if (list.size() > 0) {
			return (UserInfo) list.get(0);
		}
		return null;
	}
}