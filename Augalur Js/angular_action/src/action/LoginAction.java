package action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;




public class LoginAction{
private String name,pwd;

	public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPwd() {
	return pwd;
}

public void setPwd(String pwd) {
	this.pwd = pwd;
}

	public void execute() throws IOException{
		if (name.equals("admin")&&pwd.equals("admin")) {		
			print("success");
		}else {
			print("fail");
		}
	}
	
	private void print(String str) throws IOException {
		HttpServletResponse rep = ServletActionContext.getResponse();
		rep.setContentType("text/json;charset=utf-8");
		PrintWriter pw = rep.getWriter();
		pw.write(str);
		pw.flush();
		pw.close();
	}

}
